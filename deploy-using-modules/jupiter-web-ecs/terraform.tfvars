region                      = "ap-northeast-1"
project_name                = "aos"
vpc_cidr                    = "10.0.0.0/16"
public_subnet_1a_cidr       = "10.0.0.0/24"
public_subnet_1b_cidr       = "10.0.1.0/24"
private_app_subnet_1a_cidr  = "10.0.2.0/24"
private_app_subnet_1b_cidr  = "10.0.3.0/24"
private_data_subnet_1a_cidr = "10.0.4.0/24"
private_data_subnet_1b_cidr = "10.0.5.0/24"

domain_name      = "kops.architectprashant.com"
alternative_name = "*.kops.architectprashant.com"

container_image = "127109967130.dkr.ecr.ap-northeast-1.amazonaws.com/jupiter-web"

record_name = "www"



  
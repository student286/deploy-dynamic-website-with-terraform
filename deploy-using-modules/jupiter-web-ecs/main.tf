provider "aws" {
  region = var.region
}

# create vpc

module "vpc" {

  source                      = "../modules/vpc"
  region                      = var.region
  project_name                = var.project_name
  vpc_cidr                    = var.vpc_cidr
  public_subnet_1a_cidr       = var.public_subnet_1a_cidr
  public_subnet_1b_cidr       = var.public_subnet_1b_cidr
  private_app_subnet_1a_cidr  = var.private_app_subnet_1a_cidr
  private_app_subnet_1b_cidr  = var.private_app_subnet_1b_cidr
  private_data_subnet_1a_cidr = var.private_data_subnet_1a_cidr
  private_data_subnet_1b_cidr = var.private_data_subnet_1b_cidr

}

# create nat gateway

module "nat_gateway" {

  source                    = "../modules/nat-gateway"
  project_name              = module.vpc.project_name
  vpc_id                    = module.vpc.vpc_id
  public_subnet_1a_id       = module.vpc.public_subnet_1a_id
  public_subnet_1b_id       = module.vpc.public_subnet_1b_id
  private_app_subnet_1a_id  = module.vpc.private_app_subnet_1a_id
  private_app_subnet_1b_id  = module.vpc.private_app_subnet_1b_id
  private_data_subnet_1a_id = module.vpc.private_data_subnet_1a_id
  private_data_subnet_1b_id = module.vpc.private_data_subnet_1b_id
  internet_gateway          = module.vpc.internet_gateway

}

# create security groups

module "security_group" {

  source       = "../modules/security-groups"
  project_name = module.vpc.project_name
  vpc_id       = module.vpc.vpc_id

}

# create ecs task execution role

module "ecs_task_exec_role" {

  source       = "../modules/ecs-task-exec-role"
  project_name = module.vpc.project_name

}

# create acm ssl certificate

module "acm" {

  source           = "../modules/acm"
  domain_name      = var.domain_name
  alternative_name = var.alternative_name

}


# create application load balancer

module "load_balancer" {

  source              = "../modules/loadbalancer"
  project_name        = module.vpc.project_name
  elb_sg_id           = module.security_group.elb_sg_id
  vpc_id              = module.vpc.vpc_id
  public_subnet_1a_id = module.vpc.public_subnet_1a_id
  public_subnet_1b_id = module.vpc.public_subnet_1b_id
  certificate_arn     = module.acm.certificate_arn

}

# create ecs

module "ecs" {

  source                   = "../modules/ecs"
  region                   = module.vpc.region
  project_name             = module.vpc.project_name
  container_image          = var.container_image
  ecs_sg_id                = module.security_group.ecs_sg_id
  ecs_task_exec_role_arn   = module.ecs_task_exec_role.ecs_task_exec_role_arn
  private_app_subnet_1a_id = module.vpc.private_app_subnet_1a_id
  private_app_subnet_1b_id = module.vpc.private_app_subnet_1b_id
  target_group_arn         = module.load_balancer.target_group_arn


}

# create auto-scaling group

module "asg" {

  source           = "../modules/asg"
  ecs_cluster_name = module.ecs.ecs_cluster_name
  ecs_service_name = module.ecs.ecs_service_name

}

# create record set in route 53

module "route-53" {

  source                 = "../modules/route-53"
  domain_name            = module.acm.domain_name
  record_name            = var.record_name
  load_balancer_dns_name = module.load_balancer.load_balancer_dns_name
  load_balancer_zone_id  = module.load_balancer.load_balancer_zone_id

}

output "website_url" {

  value = join("", ["https://", var.record_name, ".", var.domain_name])

}






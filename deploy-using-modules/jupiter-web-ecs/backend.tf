terraform {
  backend "s3" {
    bucket = "aos-jupiter-ecs"
    key    = "jupiter-web-ecs.tfstate"
    region = "ap-northeast-1"
  }
}
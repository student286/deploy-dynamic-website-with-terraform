output "elb_sg_id" {
  value = aws_security_group.aos-lb-sg.id
}

output "ecs_sg_id" {
  value = aws_security_group.aos-ecs-sg.id
}

# generate an iam policy document in json format for the ecs task execution role
data "aws_iam_policy_document" "ecs-task-exec-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# create an iam role
resource "aws_iam_role" "ecs-task-exec-role" {
  name               = "${var.project_name}-ecs-exec-role"
  assume_role_policy = data.aws_iam_policy_document.ecs-task-exec-role-policy.json

}

# attach ecs task execution policy to the iam role
resource "aws_iam_role_policy_attachment" "ecs-task-exec-role" {
  role       = aws_iam_role.ecs-task-exec-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"

}
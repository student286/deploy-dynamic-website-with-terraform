variable "region" {}
variable "project_name" {}
variable "vpc_cidr" {}
variable "public_subnet_1a_cidr" {}
variable "public_subnet_1b_cidr" {}
variable "private_app_subnet_1a_cidr" {}
variable "private_app_subnet_1b_cidr" {}
variable "private_data_subnet_1a_cidr" {}
variable "private_data_subnet_1b_cidr" {}



output "region" {
  value = var.region

}

output "project_name" {
  value = var.project_name

}

output "vpc_id" {
  value = aws_vpc.aos-vpc.id

}

output "public_subnet_1a_id" {
  value = aws_subnet.aos-public-subnet-1a.id

}

output "public_subnet_1b_id" {
  value = aws_subnet.aos-public-subnet-1b.id

}

output "private_app_subnet_1a_id" {
  value = aws_subnet.aos-private-app-subnet-1a.id

}

output "private_app_subnet_1b_id" {
  value = aws_subnet.aos-private-app-subnet-1b.id

}

output "private_data_subnet_1a_id" {
  value = aws_subnet.aos-private-data-subnet-1a.id

}

output "private_data_subnet_1b_id" {
  value = aws_subnet.aos-private-data-subnet-1b.id

}


output "internet_gateway" {
  value = aws_internet_gateway.aos-igw

}









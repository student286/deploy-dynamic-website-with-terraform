data "aws_route53_zone" "hosted-zone" {
  name = var.domain_name

}

resource "aws_route53_record" "site-domain" {
  zone_id = data.aws_route53_zone.hosted-zone.zone_id
  name = var.record_name
  type = "A"

  alias {
    name = var.load_balancer_dns_name
    zone_id = var.load_balancer_zone_id
    evaluate_target_health = true
    
  }

}
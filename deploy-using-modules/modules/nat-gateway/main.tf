resource "aws_eip" "aos-eip-1a" {
  vpc = true

  tags = {
    Name = "${var.project_name}-eip-1a"

  }

}

resource "aws_eip" "aos-eip-1b" {
  vpc = true

  tags = {
    Name = "${var.project_name}-eip-1b"

  }

}

resource "aws_nat_gateway" "aos-nat-1a" {
  allocation_id = aws_eip.aos-eip-1a.id
  subnet_id     = var.public_subnet_1a_id

  tags = {
    Name = "${var.project_name}-nat-1a"
  }

  depends_on = [var.internet_gateway]

}

resource "aws_nat_gateway" "aos-nat-1b" {
  allocation_id = aws_eip.aos-eip-1b.id
  subnet_id     = var.public_subnet_1b_id

  tags = {
    Name = "${var.project_name}-nat-1b"
  }

  depends_on = [var.internet_gateway]

}

resource "aws_route_table" "aos-private-rtb-1a" {
  vpc_id = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.aos-nat-1a.id

  }

  tags = {
    Name = "${var.project_name}-private-rtb-1a"
  }
}

resource "aws_route_table_association" "private-app-rtb-asc-1a" {
  subnet_id      = var.private_app_subnet_1a_id
  route_table_id = aws_route_table.aos-private-rtb-1a.id

}

resource "aws_route_table_association" "private-data-rtb-asc-1a" {
  subnet_id      = var.private_data_subnet_1a_id
  route_table_id = aws_route_table.aos-private-rtb-1a.id

}



resource "aws_route_table" "aos-private-rtb-1b" {
  vpc_id = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.aos-nat-1b.id

  }

  tags = {
    Name = "${var.project_name}-private-rtb-1b"
  }
}

resource "aws_route_table_association" "private-app-rtb-asc-1b" {
  subnet_id      = var.private_app_subnet_1b_id
  route_table_id = aws_route_table.aos-private-rtb-1b.id

}

resource "aws_route_table_association" "private-data-rtb-asc-1b" {
  subnet_id      = var.private_data_subnet_1b_id
  route_table_id = aws_route_table.aos-private-rtb-1b.id

}








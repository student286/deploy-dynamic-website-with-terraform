variable "region" {}
variable "project_name" {}
variable "container_image" {}
variable "ecs_sg_id" {}
variable "ecs_task_exec_role_arn" {}
variable "private_app_subnet_1a_id" {}
variable "private_app_subnet_1b_id" {}
variable "target_group_arn" {}

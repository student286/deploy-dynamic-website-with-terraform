output "ecs_cluster_name" {
  value = aws_ecs_cluster.aos-ecs-cluster.name
}

output "ecs_service_name" {
  value = aws_ecs_service.aos-ecs-service.name
}


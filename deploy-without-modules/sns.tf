resource "aws_sns_topic" "user_update" {
  name = "user_update"

}

resource "aws_sns_topic_subscription" "notification_topic" {
  topic_arn = aws_sns_topic.user_update.arn
  protocol  = "email"
  endpoint  = "daprasan@outlook.com"
}
resource "aws_db_subnet_group" "aos-rds-subnet-grp" {
  name        = "aos-rds-subnet-grp"
  subnet_ids  = [aws_subnet.aos-private-data-subnet-1a.id, aws_subnet.aos-private-data-subnet-1b.id]
  description = "subnets for database instances"

  tags = {
    Name = "aos-rds-subnet-grp"
  }

}

data "aws_db_snapshot" "latest_db_snapshot" {
  db_snapshot_identifier = var.database_snapshot_arn
  most_recent            = true
  snapshot_type          = "manual"
}

resource "aws_db_instance" "aos-rds-instance" {
  instance_class         = var.database_instance_class
  skip_final_snapshot    = true
  availability_zone      = "ap-southeast-1b"
  identifier             = var.database_instance_identifier
  snapshot_identifier    = data.aws_db_snapshot.latest_db_snapshot.id
  db_subnet_group_name   = aws_db_subnet_group.aos-rds-subnet-grp.name
  multi_az               = false
  vpc_security_group_ids = [aws_security_group.aos-db-sg.id]
}
provider "aws" {
  region = "ap-southeast-1"
}

terraform {
  backend "s3" {
    bucket = "aos-terraform-state-s3"
    key    = "aos.tfstate"
    region = "ap-southeast-1"
  }
}
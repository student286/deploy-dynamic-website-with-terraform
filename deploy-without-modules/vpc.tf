resource "aws_vpc" "aos-vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "aos-vpc"
  }
}

resource "aws_internet_gateway" "aos-igw" {
  vpc_id = aws_vpc.aos-vpc.id

  tags = {
    Name = "aos-igw"
  }
}

resource "aws_subnet" "aos-public-subnet-1a" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.public_subnet_1a_cidr
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "aos-public-subnet-1a"
  }

}

resource "aws_subnet" "aos-public-subnet-1b" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.public_subnet_1b_cidr
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "aos-public-subnet-1b"
  }

}

resource "aws_route_table" "aos-public-rtb" {
  vpc_id = aws_vpc.aos-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aos-igw.id
  }

  tags = {
    Name = "aos-public-rtb"
  }

}

resource "aws_route_table_association" "rtb-asc-public-1a" {
  subnet_id      = aws_subnet.aos-public-subnet-1a.id
  route_table_id = aws_route_table.aos-public-rtb.id

}

resource "aws_route_table_association" "rtb-asc-public-1b" {
  subnet_id      = aws_subnet.aos-public-subnet-1b.id
  route_table_id = aws_route_table.aos-public-rtb.id

}

resource "aws_subnet" "aos-private-app-subnet-1a" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.private_app_subnet_1a_cidr
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "aos-private-app-subnet-1a"
  }
}

resource "aws_subnet" "aos-private-app-subnet-1b" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.private_app_subnet_1b_cidr
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "aos-private-app-subnet-1b"
  }
}

resource "aws_subnet" "aos-private-data-subnet-1a" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.private_data_subnet_1a_cidr
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "aos-private-data-subnet-1a"
  }
}

resource "aws_subnet" "aos-private-data-subnet-1b" {
  vpc_id                  = aws_vpc.aos-vpc.id
  cidr_block              = var.private_data_subnet_1b_cidr
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "aos-private-data-subnet-1b"
  }
}






resource "aws_lb" "aos-elb" {
  name               = "aos-elb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.aos-lb-sg.id]

  subnet_mapping {
    subnet_id = aws_subnet.aos-public-subnet-1a.id

  }

  subnet_mapping {
    subnet_id = aws_subnet.aos-public-subnet-1b.id

  }

  enable_deletion_protection = false

  tags = {
    Name = "aos-elb"
  }
}

resource "aws_lb_target_group" "aos-tg" {
  name        = "aos-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.aos-vpc.id

  health_check {
    healthy_threshold   = 5
    interval            = 30
    matcher             = "200,301,302"
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2

  }
}

resource "aws_lb_listener" "aos-lb-listner-http" {
  load_balancer_arn = aws_lb.aos-elb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.aos-tg.arn

  }
}



/*

resource "aws_lb_listener" "aos-lb-listner-http" {
  load_balancer_arn = aws_lb.aos-elb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      host        = "#{host}"
      path        = "/#{path}"
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"

    }
  }
}

resource "aws_lb_listener" "aos-lb-listner-https" {
  load_balancer_arn = aws_lb.aos-elb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.ssl_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.aos-tg.arn

  }
}

*/
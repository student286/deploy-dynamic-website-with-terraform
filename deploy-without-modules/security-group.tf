resource "aws_security_group" "aos-lb-sg" {
  name        = "aos-lb-sg"
  description = "aos-lb-sg"
  vpc_id      = aws_vpc.aos-vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "aos-lb-sg"
  }

}

resource "aws_security_group" "aos-ssh-sg" {
  name        = "aos-ssh-sg"
  description = "aos-ssh-sg"
  vpc_id      = aws_vpc.aos-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "aos-ssh-sg"
  }

}

resource "aws_security_group" "aos-webserver-sg" {
  name        = "aos-webserver-sg"
  description = "aos-webserver-sg"
  vpc_id      = aws_vpc.aos-vpc.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.aos-lb-sg.id]
  }

  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [aws_security_group.aos-lb-sg.id]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.aos-ssh-sg.id]
  }



  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "aos-webserver-sg"
  }

}

resource "aws_security_group" "aos-db-sg" {
  name        = "aos-db-sg"
  description = "aos-db-sg"
  vpc_id      = aws_vpc.aos-vpc.id

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.aos-webserver-sg.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "aos-db-sg"
  }

}








variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "vpc cidr block"
  type        = string

}

variable "public_subnet_1a_cidr" {
  default = "10.0.0.0/24"
  type    = string

}

variable "public_subnet_1b_cidr" {
  default = "10.0.1.0/24"
  type    = string

}

variable "private_app_subnet_1a_cidr" {
  default = "10.0.2.0/24"
  type    = string

}

variable "private_app_subnet_1b_cidr" {
  default = "10.0.3.0/24"
  type    = string

}

variable "private_data_subnet_1a_cidr" {
  default = "10.0.4.0/24"
  type    = string

}

variable "private_data_subnet_1b_cidr" {
  default = "10.0.5.0/24"
  type    = string

}

variable "database_snapshot_arn" {
  default = "arn:aws:rds:ap-southeast-1:127109967130:snapshot:aos-rds-fleetcart-snapshot"
  type    = string
}

variable "database_instance_class" {
  default = "db.t2.micro"
  type    = string
}

variable "database_instance_identifier" {
  default = "aos-rds-db"
  type    = string
}

variable "ssl_certificate_arn" {
  default = "arn:aws:acm:ap-southeast-1:127109967130:certificate/b7dcf0c2-23d6-4a57-9787-31257257a130"
  type    = string
}

variable "ami_id" {
  default = "ami-005835d578c62050d"
  type    = string
}

variable "domain_name" {
  default = "kops.architectprashant.com"
  type    = string
}

variable "record_name" {
  default = "www"
  type    = string
}






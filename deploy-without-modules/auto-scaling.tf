resource "aws_launch_template" "aos_launch_template" {
  name          = "aos_launch_template"
  image_id      = var.ami_id
  instance_type = "t2.micro"
  key_name      = "jan"

  monitoring {
    enabled = true

  }

  vpc_security_group_ids = [aws_security_group.aos-webserver-sg.id]

}

resource "aws_autoscaling_group" "aos-asg" {
  vpc_zone_identifier = [aws_subnet.aos-private-app-subnet-1a.id, aws_subnet.aos-private-app-subnet-1b.id]
  desired_capacity    = 2
  max_size            = 4
  min_size            = 1
  name                = "aos-asg"
  health_check_type   = "ELB"

  launch_template {
    name    = aws_launch_template.aos_launch_template.name
    version = "$Latest"

  }

  tag {
    key                 = "Name"
    value               = "aos-webserver"
    propagate_at_launch = true

  }

  lifecycle {
    ignore_changes = [target_group_arns]
  }
}

resource "aws_autoscaling_attachment" "aos-asg-attach" {
  autoscaling_group_name = aws_autoscaling_group.aos-asg.id
  lb_target_group_arn    = aws_lb_target_group.aos-tg.arn

}

resource "aws_autoscaling_notification" "aos-asg-notification" {
  group_names = [aws_autoscaling_group.aos-asg.name]
  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR",

  ]

  topic_arn = aws_sns_topic.user_update.arn

}
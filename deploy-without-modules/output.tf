output "website_url" {
  value = join("", [var.record_name, ".", var.domain_name])
}
resource "aws_eip" "aos-eip-nat-1a" {
  vpc = true

  tags = {
    Name = "aos-eip-nat-1a"
  }

}

resource "aws_eip" "aos-eip-nat-1b" {
  vpc = true

  tags = {
    Name = "aos-eip-nat-1b"
  }

}

resource "aws_nat_gateway" "aos-nat-1a" {
  allocation_id = aws_eip.aos-eip-nat-1a.id
  subnet_id     = aws_subnet.aos-public-subnet-1a.id

  tags = {
    Name = "aos-nat-1a"
  }

  depends_on = [aws_internet_gateway.aos-igw]

}

resource "aws_nat_gateway" "aos-nat-1b" {
  allocation_id = aws_eip.aos-eip-nat-1b.id
  subnet_id     = aws_subnet.aos-public-subnet-1b.id

  tags = {
    Name = "aos-nat-1b"
  }

  depends_on = [aws_internet_gateway.aos-igw]

}

resource "aws_route_table" "aos-private-rtb-1a" {
  vpc_id = aws_vpc.aos-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.aos-nat-1a.id

  }

  tags = {
    Name = "aos-private-rtb-1a"
  }
}

resource "aws_route_table_association" "aos-rtb-app-asc-1a" {
  subnet_id      = aws_subnet.aos-private-app-subnet-1a.id
  route_table_id = aws_route_table.aos-private-rtb-1a.id

}

resource "aws_route_table_association" "aos-rtb-data-asc-1a" {
  subnet_id      = aws_subnet.aos-private-data-subnet-1a.id
  route_table_id = aws_route_table.aos-private-rtb-1a.id

}

resource "aws_route_table" "aos-private-rtb-1b" {
  vpc_id = aws_vpc.aos-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.aos-nat-1b.id

  }

  tags = {
    Name = "aos-private-rtb-1b"
  }
}

resource "aws_route_table_association" "aos-rtb-app-asc-1b" {
  subnet_id      = aws_subnet.aos-private-app-subnet-1b.id
  route_table_id = aws_route_table.aos-private-rtb-1b.id

}

resource "aws_route_table_association" "aos-rtb-data-asc-1b" {
  subnet_id      = aws_subnet.aos-private-data-subnet-1b.id
  route_table_id = aws_route_table.aos-private-rtb-1b.id

}











